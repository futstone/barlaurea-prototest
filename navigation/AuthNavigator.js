import React from 'react'
import { createStackNavigator } from 'react-navigation'

import LoginScreen from '../screens/LoginScreen'
import RegisterScreen from '../screens/RegisterScreen'
import WelcomeScreen from '../screens/WelcomeScreen';

export default createStackNavigator({ 
  Welcome: WelcomeScreen,
  Login: LoginScreen, 
  Register: RegisterScreen 
})

