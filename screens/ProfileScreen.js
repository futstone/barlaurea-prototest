import React from 'react'
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native'

import { styles } from '../styles/styles'
import Colors from '../constants/Colors'

export default class ProfileScreen extends React.Component {
  static navigationOptions = {
    title: 'ProfileScreen',
    headerStyle: styles.headerStyle,
    headerTitleStyle: styles.headerTitleStyle
  }

  signOutAsync = () => {
    console.log("Signout clicked")
    this.props.navigation.navigate('Auth')
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>This is the ProfileScreen.js</Text>
        <Button title="Sign out." onPress={this.signOutAsync} color={Colors.buttonColor} />
      </View>
    )
  }
}