import React from 'react'
import {
  DrawerLayoutAndroid,
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableHighlight
  //Button
} from 'react-native'
import { Surface, Divider, Button, FAB } from 'react-native-paper'
import { styles } from '../styles/styles'
import Colors from '../constants/Colors'
import TabBarIcon from '../components/TabBarIcon'
import { fetchMenu } from '../data/dataservice'
import DayHeader from '../components/DayHeader'
import DayContent from '../components/DayContent'

export default class HomeScreen extends React.Component {
  constructor() {
    super()
    this.state = {
      mealoptions: [],
      headerindices: []
    }
    this.fetchMenu()
    this.openDrawer = this.openDrawer.bind(this)
  }

  static navigationOptions = {
    title: 'MenuScreen',
    headerStyle: styles.headerStyle,
    headerTitleStyle: styles.headerTitleStyle,
    headerRight: (
      <View style={styles.viewPadded}>
        <TabBarIcon name="md-menu" />
      </View>
    )
  }

  fetchMenu = async () => {
    const mealoptions = await fetchMenu()
    this.setState({ 
      mealoptions: [...mealoptions],
      headerindices: [...this.getHeaderIndices(mealoptions)] 
    })
    //console.log(mealoptions)
  }

  getHeaderIndices = (mealoptions) => {
    let indices = []
    mealoptions.forEach((day, index) => {
      if (day.header) {
        indices = indices.concat(index)
      }
    })
    return indices
  }

  openDrawer = () => {
    console.log("Do nothing..")
  }

  render() {
    console.log(this.state.headerindices)
    const navigationView = (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <Text style={{margin: 10, fontSize: 15, textAlign: 'left'}}>I'm in the Drawer!</Text>
      </View>
    )
    //console.log(this.state.mealoptions)
    return (
      <DrawerLayoutAndroid
        drawerWidth={300}
        drawerPosition={DrawerLayoutAndroid.positions.Left}
        renderNavigationView={() => navigationView}
        ref={(_drawer) => this.drawer = _drawer}
        onDrawerOpen={() => console.log("drawer is open")}>
        
          <View style={styles.containerTop}>
            <FlatList 
              stickyHeaderIndices={this.state.headerindices}
              contentContainerStyle={styles.flatList} 
              keyExtractor={(item, index) => index.toString()}
              data={[...this.state.mealoptions]}
              renderItem={({item}) => {
                if (item.header) {
                  return (
                    <DayHeader item={item} />
                  )
                } else {
                  return (
                    <DayContent item={item} />
                  )
                }
              }}
            />
          </View>

          <FAB
            color="#fff"
            style={styles.fab}
            small
            icon="keyboard-arrow-right"
            onPress={this.openDrawer}
          />

          <View style={styles.containerBottom}>
            <Button 
              style={styles.buttonFull} 
              onPress={() => console.log("pressed osta")} 
              mode="outlined" 
              color={Colors.laureaBlue} 
              icon="android" 
            >
              Osta
            </Button>
          </View>
      </DrawerLayoutAndroid>
    )
  }
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// })

{/* <View style={styles.container}>
          <Text>This is the HomeScreen.js</Text>
          <Button title="Show me more of the app." onPress={this.showMoreApp} color={Colors.buttonColor} />
          <Button title="Actually, sign me out :)" onPress={this.signOutAsync} color={Colors.buttonColor} />
        </View> */}