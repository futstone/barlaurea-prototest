import React from 'react'
import { Button } from 'react-native-paper'
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View
} from 'react-native'

import { styles } from '../styles/styles'
import Colors from '../constants/Colors'

export default class WelcomeScreen extends React.Component {
  static navigationOptions = {
    title: 'WelcomeScreen!',
  }

  login = () => {
    this.props.navigation.navigate('Login')
  }

  register = () => {
    this.props.navigation.navigate('Register')
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>This is the WelcomeScreen.js</Text>
        <Button 
          style={styles.buttonWide} 
          onPress={this.login} 
          mode="outlined" 
          color={'#fff'} 
          icon="android" 
        >
          Login
        </Button>

        <Button 
          style={styles.buttonWide} 
          onPress={this.register} 
          mode="outlined" 
          color={'#fff'} 
          icon="android" 
        >
          Register
        </Button>
      </View>
    )
  }
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// })
// <Button title="Register" onPress={this.register} color={Colors.buttonColor} />