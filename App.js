import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Provider as PaperProvider } from 'react-native-paper'

import AppNavigator from './navigation/AppNavigator'

export default class App extends React.Component {
  render() {
    return (
      <PaperProvider>
        <View style={styles.container}>
          <AppNavigator />
        </View>
      </PaperProvider>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    //alignItems: 'center',
    //justifyContent: 'center',
  },
})
