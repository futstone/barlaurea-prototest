import { StyleSheet } from 'react-native'
import layout from '../constants/Layout'
import Colors from '../constants/Colors'

export const styles = StyleSheet.create({
  container: {
    color: Colors.textColor,
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'center',
  },
  mealContainer: {
    color: Colors.textColor,
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 4,
    marginTop: 4
  },
  containerTop: {
    width: '100%',
    flex: 11,
    padding: 0,
    margin: 0,
    alignItems: 'center',
    justifyContent: 'center',
    color: Colors.textColor,
    backgroundColor: Colors.lightgray
  },
  containerBottom: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.lightgray
  },
  flatList: {
    color: Colors.textColor,
    //flex: 5,
    //flexDirection: 'column',
    width: layout.window.width
    //alignItems: 'flex-end'
  },
  headerStyle: {
    backgroundColor: Colors.headerColor
  },
  headerTitleStyle: {
    color: Colors.laureaBlue,
    fontWeight: 'normal'
  },
  buttonWide: {
    width: '100%',
    backgroundColor: '#00a0d1',
    borderColor: '#f77fb3'
  },
  buttonFull: {
    flex: 1,
    color: Colors.laureaBlue,
    borderColor: Colors.laureaBlue,
    //backgroundColor: Colors.laureaBlue,
    //alignItems: 'center',
    borderWidth: 2,
    justifyContent: 'center',
    //width: '100%'
  },
  viewPadded: {
    paddingRight: 16
  },
  surface: {
    padding: 8,
    marginTop: 8,
    marginBottom: 8,
    //height: 80,
    //alignItems: 'center',
    justifyContent: 'center',
    elevation: 6,
    color: Colors.textColor,
    backgroundColor: Colors.darkgray
  },
  dateContainer: {
    color: Colors.textColor,
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 24
  },
  dateHeaderContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    //borderBottomRightRadius: 4,
    //borderBottomLeftRadius: 4,
    //borderTopRightRadius: 4,
    //borderTopLeftRadius: 4,
    justifyContent: 'center',
    //backgroundColor: Colors.darkgray,
    color: Colors.laureaPink,
    //borderWidth: 2,
    //borderColor: Colors.laureaPink,
    //paddingTop: 4,
    //paddingBottom: 4,
    
  },
  dateHeaderText: {
    color: Colors.laureaPink,
    fontSize: 16,
    fontWeight: 'bold',
    backgroundColor: Colors.darkgray,
    paddingTop: 6,
    paddingBottom: 4,
    paddingRight: 16,
    paddingLeft: 16,
    borderBottomRightRadius: 25,
    borderBottomLeftRadius: 25,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderColor: Colors.laureaPink
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 40,
    backgroundColor: Colors.laureaBlue
  },
  ticketListItem: {
    padding: 8,
  },
  ticketListItemText: {
    fontWeight: 'bold',
    fontSize: 24
  },
  modal: {
    flex: 1,
    margin: 16,
    backgroundColor: Colors.laureaBlue,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

// darkgray: 303030
// lightgray: 383838
// laureablue: 00a0d1
// laureapink: f77fb3